#!/bin/bash

COLOR_OFF='\033[0m'

function out {
  COLOR='\033[0;35m'

  echo -e "${COLOR}[IMAGENARIUM]: $1${COLOR_OFF}"
}

set -e

export CONTAINER_ID=$(cat /proc/1/cgroup | grep "docker/" | tail -1 | sed "s/^.*\///" | cut -c 1-12)
export COMMIT_SHORT=${CI_COMMIT_SHA:0:7}
export COMMIT_DATE=$(date +"%Y_%m_%d_%H_%M")

UUID=$(cat /dev/urandom | tr -dc 'a-z' | fold -w 7 | head -n 1)

export NAMESPACE=ci${UUID}

out "Deploy traefik in namespace ${NAMESPACE}..."

response=$(http -I -j --timeout 300 --check-status \
POST http://clustercontrol:8080/groups/deploy/${NAMESPACE}/traefik/latest \
wait==true branch==${CI_COMMIT_REF_NAME} repository==${CI_PROJECT_URL} \
traefik:='{"PUBLISHED_ADMIN_PORT": "23123"}')

out "[IMAGENARIUM]: Deploy response: ${response}"

#########################################
#   sleep,   !
out "Waiting for docker network conditions..."
sleep 20                                
#########################################

set +e

out "Connect to traefik network traefik-net-${NAMESPACE}..."
docker network connect traefik-net-${NAMESPACE} ${CONTAINER_ID}

status=$?

if [[ "${status}" == 0 ]];then
  out "Integration tests...\n"

  #mvn -s ./settings.xml deploy  
  http -I --check-status GET http://traefik-${NAMESPACE}:8080/dashboard/
  status=$?

  echo -e "\n"  
  out "Integration tests finished\n"

  RED='\033[0;31m'
  GREEN='\033[0;32m'

  if [[ "${status}" == 0 ]];then
    echo -e "${GREEN}***********************"
    echo -e "${GREEN}[IMAGENARIUM]: Success"
    echo -e "${GREEN}***********************"    
    git remote set-url origin "https://gitlab-ci-token:${SECRET_TOKEN}@gitlab.com/man4j/testrepo.git"
    git tag ${CI_COMMIT_REF_NAME}-${COMMIT_DATE}-${COMMIT_SHORT} && git push --tags
  else
    echo -e "${RED}xxxxxxxxxxxxxxxxxxxxxx"
    echo -e "${RED}[IMAGENARIUM]: Failed"
    echo -e "${RED}xxxxxxxxxxxxxxxxxxxxxx"
  fi

  echo -e "${COLOR_OFF}"
fi

out "Disconnect from traefik network traefik-net-${NAMESPACE}..."
docker network disconnect -f traefik-net-${NAMESPACE} ${CONTAINER_ID}

out "Undeploy traefik from namespace ${NAMESPACE}..."
response=$(http -I -j --timeout 300 --check-status \
POST http://clustercontrol:8080/groups/undeploy/${NAMESPACE} \
wait==true)

out "Undeploy response: ${response}"

out "Done"

exit $status
